package pl.inzyniery.boardgames.appCore;

import android.app.Application;
import android.os.Handler;
import android.text.TextUtils;

import pl.inzyniery.boardgames.NativeLoader;
import pl.inzyniery.boardgames.api.engine.RestEngine;
import pl.inzyniery.boardgames.api.models.UserLookup;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class AppCore extends Application {

    private static String userName;
    private static AppCore Instance;
    public static volatile Handler applicationHandler = null;
    private static String sendURL;
    private UserLookup userLookup;

    public static String getSendURL() {
        return "http://192.168.43.177:8080/";
    }

    @Override
    public void onCreate() {
        super.onCreate();
        RestEngine.initializeRetrofitService();
        Instance=this;
        applicationHandler = new Handler(getInstance().getMainLooper());
        NativeLoader.initNativeLibs(AppCore.getInstance());
    }

    public static String getUserName() {
        if (!TextUtils.isEmpty(userName)) {
            return userName;
        } else return "";
    }

    public static void setUserName(String userName) {
        AppCore.userName = userName;
    }

    public static AppCore getInstance()
    {
        return Instance;
    }

    public UserLookup getUserLookup() {
        return userLookup;
    }

    public void setUserLookup(UserLookup userLookup) {
        this.userLookup = userLookup;
    }
}
