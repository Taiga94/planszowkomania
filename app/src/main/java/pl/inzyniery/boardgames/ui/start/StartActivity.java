package pl.inzyniery.boardgames.ui.start;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.dd.processbutton.iml.ActionProcessButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.ui.base.BaseActivity;
import pl.inzyniery.boardgames.ui.login.LoginActivity;
import pl.inzyniery.boardgames.ui.main.MainActivity;
import pl.inzyniery.boardgames.ui.register.RegisterActivity;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class StartActivity extends BaseActivity implements StartPresenter.ViewController, View.OnClickListener {

    private ViewHolder viewHolder;
    private StartPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        viewHolderSetUp();
        presenterSetUp();
    }

    @Override
    public void startRegister() {
        Intent intent = new Intent(StartActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void startLoginActivity() {
        Intent intent = new Intent(StartActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void startProgressDemoButton() {
        viewHolder.demo.setProgress(1);
    }

    @Override
    public void stopProgressDemoButton() {
        viewHolder.demo.setProgress(0);
    }

    @Override
    public void onErrorProgressButton() {
        viewHolder.demo.setProgress(-1);
    }

    @Override
    public void clearStateDemoButton() {
        viewHolder.demo.setProgress(0);
    }

    @Override
    protected void viewHolderSetUp() {
        viewHolder = new ViewHolder(getActivityView());
    }

    @Override
    protected void presenterSetUp() {
        presenter = new StartPresenter();
        presenter.onLoad(this);
    }

    @Override
    public void initView() {
        viewHolder.login.setOnClickListener(this);
        viewHolder.demo.setOnClickListener(this);
        viewHolder.register.setOnClickListener(this);
        viewHolder.demo.setMode(ActionProcessButton.Mode.ENDLESS);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == viewHolder.login.getId()) {
            presenter.onLoginClicked();
        } else if (view.getId() == viewHolder.register.getId()) {
            presenter.onRegisterClicked();
        } else if (view.getId() == viewHolder.demo.getId()) {
            presenter.onDemoClicked();
        }
    }

    static class ViewHolder {
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @BindView(R.id.activity_start_button_register)
        Button register;

        @BindView(R.id.activity_start_button_login)
        Button login;

        @BindView(R.id.activity_start_button_demo)
        ActionProcessButton demo;
    }
}
