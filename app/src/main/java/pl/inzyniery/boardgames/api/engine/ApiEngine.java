package pl.inzyniery.boardgames.api.engine;

import com.google.gson.Gson;
import okhttp3.ResponseBody;
import pl.inzyniery.boardgames.api.interfaces.GetUsersLookupListener;
import pl.inzyniery.boardgames.api.interfaces.PositionSendListener;
import pl.inzyniery.boardgames.api.interfaces.RegisterListener;
import pl.inzyniery.boardgames.api.interfaces.UniversalLoginListener;
import pl.inzyniery.boardgames.api.request.GetUsersGPSRequest;
import pl.inzyniery.boardgames.api.request.GetUsersLookupRequest;
import pl.inzyniery.boardgames.api.request.LoginNoAccountRequest;
import pl.inzyniery.boardgames.api.request.LoginRequest;
import pl.inzyniery.boardgames.api.request.PositionRequest;
import pl.inzyniery.boardgames.api.request.RegisterRequest;
import pl.inzyniery.boardgames.api.response.GetUsersLookupResponse;
import pl.inzyniery.boardgames.api.response.UniversalLoginRegisterResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄ ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀ ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class ApiEngine {

    public static void sendLoginWithNoAccount(String uid, final UniversalLoginListener listener) {
        listener.onTaskStart();
        LoginNoAccountRequest request = new LoginNoAccountRequest(uid);
        Call<ResponseBody> call;
        call = RestEngine.getApiInterface().sendPostLoginRequest(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onTaskEnd();
                try {
                    UniversalLoginRegisterResponse resp;
                    resp = new Gson().fromJson(response.body().string(), UniversalLoginRegisterResponse.class);
                    listener.onSuccess(resp);
                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onFailure();
            }
        });
    }

    public static void sendRegister(String login, String password, String email, final RegisterListener listener) {
        listener.onTaskStart();
        RegisterRequest request = new RegisterRequest(login, password, email);
        Call<ResponseBody> call;
        call = RestEngine.getApiInterface().sendPostRegisterRequest(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onTaskEnd();
                listener.onSuccess();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onTaskEnd();
                listener.onFailure();
            }
        });
    }

    public static void sendLogin(String login, String password, final UniversalLoginListener listener) {
        listener.onTaskStart();
        LoginRequest request = new LoginRequest(login, password);
        Call<ResponseBody> call;
        call = RestEngine.getApiInterface().sendPostLogin(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onTaskEnd();
                try {
                    UniversalLoginRegisterResponse resp;
                    resp = new Gson().fromJson(response.body().string(), UniversalLoginRegisterResponse.class);
                    listener.onSuccess(resp);
                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onTaskEnd();
                listener.onFailure();
            }
        });
    }

    public static void sendPosition(double lat, double lon, final PositionSendListener listener) {
        PositionRequest request = new PositionRequest(lat, lon);
        Call<ResponseBody> call;
        call = RestEngine.getApiInterface().sendPostPosition(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onSuccess();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onFail();
            }
        });
    }

    public static void sendGetUsersLookup(String name, final GetUsersLookupListener listener) {
        listener.onTaskStart();
        final GetUsersLookupRequest request = new GetUsersLookupRequest(name);
        Call<ResponseBody> call;
        call = RestEngine.getApiInterface().sendPostGetUsersLookup(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onTaskEnd();
                GetUsersLookupResponse resp;
                try {
                    resp = new Gson().fromJson(response.body().string(), GetUsersLookupResponse.class);
                    listener.onSuccess(resp);
                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onTaskEnd();
                listener.onFailure();
            }
        });
    }

    public static void getUsersGPS(final double distance, final GetUsersLookupListener listener) {
        listener.onTaskStart();
        final GetUsersGPSRequest request = new GetUsersGPSRequest(distance);
        Call<ResponseBody> call;
        call = RestEngine.getApiInterface().getUsersGPS(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onTaskEnd();
                GetUsersLookupResponse resp;
                try {
                    resp = new Gson().fromJson(response.body().string(), GetUsersLookupResponse.class);
                    listener.onSuccess(resp);
                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onTaskEnd();
                listener.onFailure();
            }
        });
    }
}
