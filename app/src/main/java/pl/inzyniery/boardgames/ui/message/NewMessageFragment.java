package pl.inzyniery.boardgames.ui.message;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.dd.processbutton.iml.ActionProcessButton;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.api.engine.RestEngine;
import pl.inzyniery.boardgames.api.request.AddChatRequest;
import pl.inzyniery.boardgames.api.response.AddChatResponse;
import pl.inzyniery.boardgames.appCore.AppCore;
import pl.inzyniery.boardgames.ui.chat.ChatActivity;
import pl.inzyniery.boardgames.ui.chat.WebService;
import pl.inzyniery.boardgames.ui.search.SearchActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class NewMessageFragment extends Fragment {
    @BindView(R.id.fragment_new_message_add_friends_id)
    public ActionProcessButton btAddFriends;
    @BindView(R.id.fragment_new_message_send_id)
    public ActionProcessButton btSendMessage;

    private WebService webService;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_message, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        buildRetrofit();
        btAddFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
            }
        });
        btSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddChatRequest addChatRequest = new AddChatRequest();
                addChatRequest.setName("fajny czat");
                final String headerInfo = RestEngine.getToken();

                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                                Request request = chain.request();
                                request = request.newBuilder()
                                        .addHeader("token", headerInfo)
                                        .build();
                                okhttp3.Response response = chain.proceed(request);
                                return response;
                            }
                        })
                        .addInterceptor(interceptor)
                        .build();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(RestEngine.URL)
                        .addConverterFactory(GsonConverterFactory.create(new Gson()))
                        .client(client)
                        .build();
                WebService webService = retrofit.create(WebService.class);
                Call<AddChatResponse> call = webService.addChat(addChatRequest);
                call.enqueue(new Callback<AddChatResponse>() {
                    @Override
                    public void onResponse(Call<AddChatResponse> call, Response<AddChatResponse> response) {
                        Intent intent = new Intent(getActivity(), ChatActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<AddChatResponse> call, Throwable t) {

                    }
                });

            }
        });
    }

    private void buildRetrofit() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestEngine.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        webService = retrofit.create(WebService.class);
    }
}
