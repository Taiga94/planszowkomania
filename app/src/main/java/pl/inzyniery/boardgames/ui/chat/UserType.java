package pl.inzyniery.boardgames.ui.chat;

/**
 * Created by madhur on 17/01/15.
 */
public enum UserType {
    OTHER, SELF
};
