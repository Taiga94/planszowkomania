package pl.inzyniery.boardgames.ui.duel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.ui.base.BaseActivity;
import pl.inzyniery.boardgames.ui.search.SearchActivity;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class DuelActivity extends BaseActivity implements DuelPresenter.ViewController {
    ViewHolder viewHolder;
    DuelPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duel);
        viewHolderSetUp();
        presenterSetUp();
        initView();

    }

    @Override
    protected void viewHolderSetUp() {
        viewHolder = new ViewHolder(getActivityView());
    }

    @Override
    protected void presenterSetUp() {
        presenter = new DuelPresenter();
        presenter.onLoad(this);
    }

    @Override
    public void initView() {
        viewHolder.btSearchRival.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onBtSearchRival();
            }
        });

    }

    @Override
    public void searchRivalClick() {
        Intent intent = new Intent(DuelActivity.this, SearchActivity.class);
        startActivity(intent);
    }

    static class ViewHolder {
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @BindView(R.id.activity_duel_bt_choose_rival)
        View btSearchRival;
    }
}