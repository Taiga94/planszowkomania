package pl.inzyniery.boardgames.ui.search;

import java.util.ArrayList;
import java.util.List;

import pl.inzyniery.boardgames.api.engine.ApiEngine;
import pl.inzyniery.boardgames.api.interfaces.GetUsersLookupListener;
import pl.inzyniery.boardgames.api.models.UserLookup;
import pl.inzyniery.boardgames.api.response.GetUsersLookupResponse;
import pl.inzyniery.boardgames.ui.base.BasePresenter;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class SearchPresenter extends BasePresenter<SearchActivity> {

    protected interface ViewController {
        void initView();

        void initRangeAdapter(List<String> items);

        void updateUserFromLookupsList(List<UserLookup> items);

        void updateUserFromGPSList(List<UserLookup> items);

        void setGoneVisibilityForList();
    }

    @Override
    public void onLoad(SearchActivity view) {
        super.onLoad(view);
        getView().initView();
        initList();
        updateListFromGPS();
    }

    private void initList() {
        List<String> area = new ArrayList<>();
        area.add("Znajomi w pokoju");
        area.add("Znajomi w 1 km");
        area.add("Znajomi w 5 km");
        getView().initRangeAdapter(area);
    }

    public void updateListFromLookups(String s) {
        ApiEngine.sendGetUsersLookup(s, new GetUsersLookupListener() {
            @Override
            public void onSuccess(GetUsersLookupResponse response) {
                getView().updateUserFromLookupsList(response.getUserLookups());
            }

            @Override
            public void onTaskStart() {

            }

            @Override
            public void onTaskEnd() {

            }

            @Override
            public void onFailure() {
            }
        });
    }

    public void updateListFromGPS() {
        ApiEngine.getUsersGPS(0.2, new GetUsersLookupListener() {
            @Override
            public void onSuccess(GetUsersLookupResponse response) {
                getView().updateUserFromGPSList(response.getUserLookups());
            }

            @Override
            public void onTaskStart() {

            }

            @Override
            public void onTaskEnd() {

            }

            @Override
            public void onFailure() {
            }
        });
    }

    public void onBtSearchClicked() {
        getView().showInDevToast();
    }

    public void onBtWriteFriend() {
    }


}

