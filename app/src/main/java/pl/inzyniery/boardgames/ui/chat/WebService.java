package pl.inzyniery.boardgames.ui.chat;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

import pl.inzyniery.boardgames.api.request.AddChatRequest;
import pl.inzyniery.boardgames.api.request.ChatDetailsRequest;
import pl.inzyniery.boardgames.api.request.ChatListRequest;
import pl.inzyniery.boardgames.api.response.AddChatResponse;
import pl.inzyniery.boardgames.api.response.ChatDetailsResponse;
import pl.inzyniery.boardgames.api.response.Chats;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface WebService {
    @POST("/planszowkomania/addChat")
    Call<AddChatResponse> addChat (@Body AddChatRequest AddChatRequest);

    @POST("/planszowkomania/getChatList")
    Call <Chats> getChatList (@Body ChatListRequest chatListRequest);

    @POST("/planszowkomania/api/getChatDetails")
    Call <ChatDetailsResponse> getChatDetails (@Body ChatDetailsRequest chatDetailsRequest);




}