package pl.inzyniery.boardgames.ui.register;

import android.text.TextUtils;

import pl.inzyniery.boardgames.api.engine.ApiEngine;
import pl.inzyniery.boardgames.api.interfaces.RegisterListener;
import pl.inzyniery.boardgames.ui.base.BasePresenter;


/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class RegisterPresenter extends BasePresenter<RegisterActivity> {


    protected interface ViewController {
        void initView();

        void fillInFieldsInfo();

        void setButtonProgress();

        void setButtonError();

        void stopButtonProgress();

        void showSuccessDialog();

    }

    @Override
    public void onLoad(RegisterActivity view) {
        super.onLoad(view);
        getView().initView();
    }

    public void onBtRegisterClicked(String login, String password, String email) {
        if (TextUtils.isEmpty(login) || TextUtils.isEmpty(password) || TextUtils.isEmpty(email)) {
            getView().fillInFieldsInfo();
            return;
        }

        ApiEngine.sendRegister(login, password, email, new RegisterListener() {
            @Override
            public void onSuccess() {
                getView().showSuccessDialog();
            }

            @Override
            public void onTaskStart() {
                getView().setButtonProgress();
            }

            @Override
            public void onTaskEnd() {
                getView().stopButtonProgress();
            }

            @Override
            public void onFailure() {
                getView().setButtonError();
            }
        });
    }
}
