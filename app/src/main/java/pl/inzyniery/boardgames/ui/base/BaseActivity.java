package pl.inzyniery.boardgames.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.api.engine.RestEngine;
import pl.inzyniery.boardgames.widgets.CustomToast;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        RestEngine.setToken("");
    }

    protected abstract void viewHolderSetUp();

    protected abstract void presenterSetUp();

    protected View getActivityView() {
        return findViewById(android.R.id.content);
    }

    protected void showNegativeToast(String text) {
        CustomToast.showNegativeToast(text, this);
    }

    protected void showNegativeToast(int resourceId) {
        CustomToast.showNegativeToast(resourceId, this);
    }

    protected void showPositiveToast(String text) {
        CustomToast.showPositiveToast(text, this);
    }

    protected void showPositiveToast(int resourceId) {
        CustomToast.showPositiveToast(resourceId, this);
    }

    protected void showNegativeToast(String text, boolean immediatelyShowToast) {
        CustomToast.showNegativeToast(text, immediatelyShowToast, this);
    }

    protected void showNegativeToast(int resourceId, boolean immediatelyShowToast) {
        CustomToast.showNegativeToast(resourceId, immediatelyShowToast, this);
    }

    protected void showPositiveToast(String text, boolean immediatelyShowToast) {
        CustomToast.showPositiveToast(text, immediatelyShowToast, this);
    }

    protected void showPositiveToast(int resourceId, boolean immediatelyShowToast) {
        CustomToast.showPositiveToast(resourceId, immediatelyShowToast, this);
    }

    public void showInDevToast() {
        Toast.makeText(this, R.string.in_dev, Toast.LENGTH_SHORT).show();
    }

    protected void setViewRippleEffect(View view) {
        MaterialRippleLayout.on(view).rippleColor(R.color.button_pressed).
                rippleHover(true).rippleAlpha(0.1f).rippleOverlay(true).rippleDelayClick(false).create();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
