package pl.inzyniery.boardgames.ui.base;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.widgets.CustomToast;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public abstract class BaseFragment extends Fragment {

    protected abstract void viewHolderSetUp(View view);

    protected abstract void presenterSetUp();

    protected void showNegativeToast(String text) {
        CustomToast.showNegativeToast(text, getContext());
    }

    protected void showNegativeToast(int resourceId) {
        CustomToast.showNegativeToast(resourceId, getContext());
    }

    protected void showPositiveToast(String text) {
        CustomToast.showPositiveToast(text, getContext());
    }

    protected void showPositiveToast(int resourceId) {
        CustomToast.showPositiveToast(resourceId, getContext());
    }

    protected void showNegativeToast(String text, boolean immediatelyShowToast) {
        CustomToast.showNegativeToast(text, immediatelyShowToast, getContext());
    }

    protected void showNegativeToast(int resourceId, boolean immediatelyShowToast) {
        CustomToast.showNegativeToast(resourceId, immediatelyShowToast, getContext());
    }

    protected void showPositiveToast(String text, boolean immediatelyShowToast) {
        CustomToast.showPositiveToast(text, immediatelyShowToast, getContext());
    }

    protected void showPositiveToast(int resourceId, boolean immediatelyShowToast) {
        CustomToast.showPositiveToast(resourceId, immediatelyShowToast, getContext());
    }

    public void showInDevToast() {
        Toast.makeText(getContext(), R.string.in_dev, Toast.LENGTH_SHORT).show();
    }

    protected void setViewRippleEffect(View view) {
        MaterialRippleLayout.on(view).rippleColor(R.color.button_pressed).
                rippleHover(true).rippleAlpha(0.1f).rippleOverlay(true).rippleDelayClick(false).create();
    }
}
