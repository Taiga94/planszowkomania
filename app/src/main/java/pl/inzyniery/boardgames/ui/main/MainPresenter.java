package pl.inzyniery.boardgames.ui.main;

import java.util.ArrayList;

import pl.inzyniery.boardgames.ui.base.BasePresenter;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class MainPresenter extends BasePresenter<MainActivity> {

    protected interface ViewController {
        void initView();

        void initGridViewGames(GamesAdapter gamesAdapter);

        void showExitAppDialog();

        void rankingClick();

        void duelClick();

        void clickTournament();

        ArrayList<ItemGame> getGridViewGamesItems();

        void startLocationService();

    }

    @Override
    public void onLoad(MainActivity view) {
        super.onLoad(view);
        getView().initView();
        getView().initGridViewGames(
                new GamesAdapter(
                        getView(), getView().getGridViewGamesItems()
                )
        );
    }

    public void onBackPressedClicked() {
        getView().showExitAppDialog();
    }

    public void onRankingClicked() {
        getView().rankingClick();
    }

    public void onSettingsClicked() {
        getView().rankingClick();
    }

    public void onLogoutClicked() {
        getView().showExitAppDialog();
    }

    public void onDuelClicked() {
        getView().duelClick();
    }

    public void onTournamentClicked() {
        getView().clickTournament();
    }
}
