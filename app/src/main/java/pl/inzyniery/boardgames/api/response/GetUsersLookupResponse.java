package pl.inzyniery.boardgames.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import pl.inzyniery.boardgames.api.models.UserLookup;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class GetUsersLookupResponse {

    @SerializedName("data")
    List<UserLookup> userLookups;

    public GetUsersLookupResponse(ArrayList<UserLookup> userLookups) {
        this.userLookups = userLookups;
    }

    public List<UserLookup> getUserLookups() {
        return userLookups;
    }

    public void setUserLookups(ArrayList<UserLookup> userLookups) {
        this.userLookups = userLookups;
    }
}
