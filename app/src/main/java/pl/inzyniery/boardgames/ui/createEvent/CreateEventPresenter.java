package pl.inzyniery.boardgames.ui.createEvent;

import java.util.Calendar;

import pl.inzyniery.boardgames.ui.base.BasePresenter;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class CreateEventPresenter extends BasePresenter<CreateEventActivity> {
    protected interface ViewController {
        void initView();

        void showDateDialog();

        void showTimeDialog();

        void showInviteFriendActivity();

        void showFillInDataInfo();

        void searchFriendClick();

        void setDate(Calendar date);

        void setDate(Calendar date, int hour, int minute);
    }

    @Override
    public void onLoad(CreateEventActivity view) {
        super.onLoad(view);
        getView().initView();
        getView().setDate(Calendar.getInstance());
    }

    public void onClickDateButton() {
        getView().showDateDialog();

    }

    public void onClickInviteFriendsButton() {

    }

    public void onClickCreateEventButton() {

    }

    public void onSelectedDate() {

    }

    public void onSelectedTime() {

    }

    public void onInvitedFriends() {

    }

    public void onBtAddFriends() {
        getView().searchFriendClick();
    }
}
