package pl.inzyniery.boardgames.api.response;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

import java.io.Serializable;

/**
 * Created by Daniel on 01.01.2017.
 */
public class MessageResponse implements Serializable {
    String textMessage;
    String time;
    String author;


    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public MessageResponse(String author, String textMessage, String time) {

        this.textMessage = textMessage;
        this.time = time;
    }

    public MessageResponse() {

    }
}