package pl.inzyniery.boardgames.ui.ranking;

import pl.inzyniery.boardgames.ui.base.BasePresenter;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class RankingPresenter extends BasePresenter<RankingActivity> {

    protected interface ViewController {
        void initView();

    }

    @Override
    public void onLoad(RankingActivity view) {
        super.onLoad(view);
    }

    public void onBtRegisterClicked() {
        getView().showInDevToast();
    }
}

