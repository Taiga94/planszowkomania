package pl.inzyniery.boardgames.ui.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.appCore.AppCore;
import pl.inzyniery.boardgames.services.PositionGetService;
import pl.inzyniery.boardgames.ui.base.BaseActivity;
import pl.inzyniery.boardgames.ui.tournament.TournamentActivity;
import pl.inzyniery.boardgames.util.ServiceUtils;
import pl.inzyniery.boardgames.ui.duel.DuelActivity;
import pl.inzyniery.boardgames.ui.ranking.RankingActivity;
import pl.inzyniery.boardgames.util.ServiceUtils;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class MainActivity extends BaseActivity
        implements MainPresenter.ViewController, View.OnClickListener {

    private ViewHolder viewHolder;
    private MainPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewHolderSetUp();
        presenterSetUp();
    }

    @Override
    protected void viewHolderSetUp() {
        viewHolder = new ViewHolder(getActivityView());
    }

    @Override
    protected void presenterSetUp() {
        presenter = new MainPresenter();
        presenter.onLoad(this);
    }

    @Override
    public void initView() {
        viewHolder.tvName.setText(AppCore.getUserName());
        viewHolder.btSettings.setOnClickListener(this);
        viewHolder.btRanking.setOnClickListener(this);
        viewHolder.btLogout.setOnClickListener(this);
        setViewRippleEffect(viewHolder.btLogout);
        setViewRippleEffect(viewHolder.btRanking);
        setViewRippleEffect(viewHolder.btSettings);
        startServiceLocationGet();
    }

    private void startServiceLocationGet() {
        Intent intent;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
        } else {
            intent = new Intent(this, PositionGetService.class);
            startService(intent);
        }
    }

    @Override
    public void initGridViewGames(GamesAdapter adapter) {
        viewHolder.gvGames.setAdapter(adapter);
    }

    @Override
    public void showExitAppDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.do_you_rly_want_exit)
                .content(R.string.do_you_rly_want_exit_2)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        MainActivity.this.finishAffinity();
                    }
                })
                .show();
    }

    @Override
    public void rankingClick() {
        Intent intent = new Intent(MainActivity.this, RankingActivity.class);
        startActivity(intent);
    }

    public void duelClick() {
        Intent intent = new Intent(MainActivity.this, DuelActivity.class);
        startActivity(intent);
    }

    public void clickTournament() {
        Intent intent = new Intent(MainActivity.this, TournamentActivity.class);
        startActivity(intent);
    }

    @Override
    public ArrayList<ItemGame> getGridViewGamesItems() {
        ArrayList<ItemGame> itemGames = new ArrayList<>();
        itemGames.add(new ItemGame(R.drawable.icon_tournament, getString(R.string.tournament)));
        itemGames.add(new ItemGame(R.drawable.icon_tournament_add, getString(R.string.duel)));
        itemGames.add(new ItemGame(R.drawable.random_game_ic, getString(R.string.random_game)));
        itemGames.add(new ItemGame(R.drawable.icon_calendar, getString(R.string.create_event)));
        itemGames.add(new ItemGame(R.drawable.ico_message, getString(R.string.message)));
        return itemGames;
    }

    @Override
    public void startLocationService() {
        if (ServiceUtils.isMyServiceRunning(PositionGetService.class, this)) {
            Intent intent = new Intent(this, PositionGetService.class);
            startService(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED) {
                        startServiceLocationGet();
                    }
                    startLocationService();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressedClicked();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == viewHolder.btLogout.getId()) {
            presenter.onLogoutClicked();
        } else if (view.getId() == viewHolder.btRanking.getId()) {
            presenter.onRankingClicked();
        } else if (view.getId() == viewHolder.btSettings.getId()) {
            presenter.onSettingsClicked();
        } else {
            showInDevToast();
        }
    }


    static class ViewHolder {
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @BindView(R.id.activity_main_tv_name)
        TextView tvName;

        @BindView(R.id.activity_main_bt_logout)
        Button btLogout;

        @BindView(R.id.activity_main_bt_ranking)
        Button btRanking;

        @BindView(R.id.activity_main_bt_settings)
        Button btSettings;

        @BindView(R.id.activity_main_gv)
        GridView gvGames;
    }
}
