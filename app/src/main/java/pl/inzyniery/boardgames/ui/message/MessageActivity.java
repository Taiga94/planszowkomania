package pl.inzyniery.boardgames.ui.message;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewParent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.ui.base.BaseActivity;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class MessageActivity extends BaseActivity implements MessagePresenter.ViewController {
    private ViewHolder viewHolder;
    private MessagePresenter presenter;
    private List<Fragment> fragments = new ArrayList<>();
    private MessageActivityAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        viewHolderSetUp();
        presenterSetUp();
    }

    @Override
    protected void viewHolderSetUp() {
        viewHolder = new ViewHolder(getActivityView());

    }

    @Override
    protected void presenterSetUp() {
        presenter = new MessagePresenter();
        presenter.onLoad(this);
    }

    @Override
    public void initView() {
        fragments.add(new NewMessageFragment());
        fragments.add(new ListOfMessageFragment());
        adapter = new MessageActivityAdapter(getSupportFragmentManager(), fragments);
        viewHolder.viewPager.setAdapter(adapter);
    }


    static class ViewHolder {
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @BindView(R.id.message_pager_id)
        ViewPager viewPager;
    }
}
