package pl.inzyniery.boardgames.api.interfaces;

import okhttp3.ResponseBody;
import pl.inzyniery.boardgames.api.request.GetUsersGPSRequest;
import pl.inzyniery.boardgames.api.request.GetUsersLookupRequest;
import pl.inzyniery.boardgames.api.request.LoginNoAccountRequest;
import pl.inzyniery.boardgames.api.request.LoginRequest;
import pl.inzyniery.boardgames.api.request.PositionRequest;
import pl.inzyniery.boardgames.api.request.RegisterRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public interface ApiRestInterface {

    @POST("anonymousLogin")
    Call<ResponseBody> sendPostLoginRequest(@Body LoginNoAccountRequest request);

    @POST("register")
    Call<ResponseBody> sendPostRegisterRequest(@Body RegisterRequest request);

    @POST("login")
    Call<ResponseBody> sendPostLogin(@Body LoginRequest request);

    @POST("updatePosition")
    Call<ResponseBody> sendPostPosition(@Body PositionRequest request);

    @POST("getUsersLookup")
    Call<ResponseBody> sendPostGetUsersLookup(@Body GetUsersLookupRequest request);

    @POST("getUsersGPS")
    Call<ResponseBody> getUsersGPS(@Body GetUsersGPSRequest request);
}
