package pl.inzyniery.boardgames.api.interfaces;

import pl.inzyniery.boardgames.api.response.GetUsersLookupResponse;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public interface GetUsersLookupListener extends SimpleApiListener {
    void onSuccess(GetUsersLookupResponse response);
}
