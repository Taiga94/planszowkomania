package pl.inzyniery.boardgames.ui.message;

import pl.inzyniery.boardgames.ui.base.BasePresenter;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class MessagePresenter extends BasePresenter<MessageActivity> {
    protected interface ViewController {
        void initView();
    }


    @Override
    public void onLoad(MessageActivity view) {
        super.onLoad(view);
        getView().initView();
    }


}
