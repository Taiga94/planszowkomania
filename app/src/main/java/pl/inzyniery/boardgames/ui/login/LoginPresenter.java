package pl.inzyniery.boardgames.ui.login;

import android.content.Intent;
import android.text.TextUtils;

import pl.inzyniery.boardgames.api.interfaces.UniversalLoginListener;
import pl.inzyniery.boardgames.api.engine.ApiEngine;
import pl.inzyniery.boardgames.api.engine.RestEngine;
import pl.inzyniery.boardgames.api.response.UniversalLoginRegisterResponse;
import pl.inzyniery.boardgames.appCore.AppCore;
import pl.inzyniery.boardgames.ui.base.BasePresenter;
import pl.inzyniery.boardgames.ui.main.MainActivity;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class LoginPresenter extends BasePresenter<LoginActivity> {

    protected interface ViewController {
        void initView();

        void setButtonProgress();

        void stopButtonProgress();

        void showErrorButton();

        void showFillInDataInfo();
    }

    @Override
    public void onLoad(LoginActivity view) {
        super.onLoad(view);
        getView().initView();
    }

    public void onFbBtLogin() {
        getView().showInDevToast();
    }

    public void onBtLoginClicked(String login, String password) {
        if (TextUtils.isEmpty(login) || TextUtils.isEmpty(password)) {
            getView().showFillInDataInfo();
            return;
        }

        ApiEngine.sendLogin(login, password, new UniversalLoginListener() {
            @Override
            public void onSuccess(UniversalLoginRegisterResponse response) {
                RestEngine.setToken(response.getToken());
                AppCore.setUserName(response.getName());
                Intent intent = new Intent(getView(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getView().startActivity(intent);

            }

            @Override
            public void onTaskStart() {
                getView().setButtonProgress();
            }

            @Override
            public void onTaskEnd() {
                getView().stopButtonProgress();
            }

            @Override
            public void onFailure() {
                getView().showErrorButton();
            }
        });
    }
}
