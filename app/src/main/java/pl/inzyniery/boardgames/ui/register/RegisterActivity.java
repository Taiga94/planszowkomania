package pl.inzyniery.boardgames.ui.register;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dd.processbutton.iml.ActionProcessButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.ui.base.BaseActivity;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class RegisterActivity extends BaseActivity implements RegisterPresenter.ViewController {

    private ViewHolder viewHolder;
    private RegisterPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        viewHolderSetUp();
        presenterSetUp();
    }

    @Override
    protected void viewHolderSetUp() {
        viewHolder = new ViewHolder(getActivityView());
    }

    @Override
    protected void presenterSetUp() {
        presenter = new RegisterPresenter();
        presenter.onLoad(this);
    }

    @Override
    public void initView() {
        viewHolder.btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onBtRegisterClicked(
                        viewHolder.etLogin.getText().toString(),
                        viewHolder.etPassword.getText().toString(),
                        viewHolder.etEmail.getText().toString());
            }
        });
        setViewRippleEffect(viewHolder.btRegister);
        viewHolder.btRegister.setMode(ActionProcessButton.Mode.ENDLESS);
    }

    @Override
    public void fillInFieldsInfo() {
        showNegativeToast(R.string.empty_data);
    }

    @Override
    public void setButtonProgress() {
        viewHolder.btRegister.setProgress(1);
    }

    @Override
    public void setButtonError() {
        viewHolder.btRegister.setProgress(-1);
    }

    @Override
    public void stopButtonProgress() {
        viewHolder.btRegister.setProgress(0);
    }

    @Override
    public void showSuccessDialog() {
        new MaterialDialog.Builder(this)
                .positiveText(R.string.ok)
                .title(R.string.congratulations)
                .content(R.string.check_mail)
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .show();
    }

    static class ViewHolder {
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @BindView(R.id.register_activity_button_register)
        ActionProcessButton btRegister;

        @BindView(R.id.activity_register_et_email)
        EditText etEmail;

        @BindView(R.id.activity_register_et_login)
        EditText etLogin;

        @BindView(R.id.activity_register_et_password)
        EditText etPassword;
    }
}

