package pl.inzyniery.boardgames.api.engine;

import android.text.TextUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.inzyniery.boardgames.api.interfaces.ApiRestInterface;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class RestEngine {
    //212.182.18.244
    //192.168.1.233
    //87.246.221.128
    public static final String URL = "http://192.168.43.177:8080/planszowkomania/";
    private static OkHttpClient.Builder httpClient;
    private static final long CONNECTION_TIMEOUT_IN_SEC = 5;
    private static ApiRestInterface apiService;
    private static String token;

    public static void initializeRetrofitService() {
        initializeHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        apiService = retrofit.create(ApiRestInterface.class);
    }

    private static void initializeHttpClient() {
        httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(CONNECTION_TIMEOUT_IN_SEC, TimeUnit.SECONDS).build();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(interceptor);
        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request;

                if (!TextUtils.isEmpty(token)) {
                    request = original.newBuilder()
                            .addHeader("token", token)
                            .method(original.method(), original.body())
                            .build();
                } else {
                    request = original.newBuilder()
                            .method(original.method(), original.body())
                            .build();
                }

                return chain.proceed(request);
            }
        });
    }

    public static ApiRestInterface getApiInterface() {
        return apiService;
    }

    public static void setToken(String token) {
        if (!TextUtils.isEmpty(token))
            RestEngine.token = token;
    }

    public static String getToken() {
        return token;
    }
}
