package pl.inzyniery.boardgames.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.api.models.UserLookup;
import pl.inzyniery.boardgames.ui.base.BaseActivity;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class SearchActivity extends BaseActivity implements SearchPresenter.ViewController {

    ViewHolder viewHolder;
    SearchPresenter presenter;

    private UserSearchResultAdapter adapter;

    private List<UserLookup> users = new ArrayList<>();

    private List<UserLookup> usersLookups = new ArrayList<>();

    private List<UserLookup> usersLookupsGPS = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        setContentView(R.layout.activity_search);
        viewHolderSetUp();
        presenterSetUp();
    }

    @Override
    protected void viewHolderSetUp() {
        viewHolder = new ViewHolder(getActivityView());
    }

    @Override
    protected void presenterSetUp() {
        presenter = new SearchPresenter();
        presenter.onLoad(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

    @Override
    public void initView() {
        viewHolder.btFromFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onBtSearchClicked();
            }
        });

        viewHolder.newFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onBtWriteFriend();
            }
        });
        viewHolder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

        });
        setViewRippleEffect(viewHolder.btFromFb);
        viewHolder.newFriend.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.updateListFromLookups(editable.toString());
            }
        });

        adapter = new UserSearchResultAdapter(users);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        viewHolder.recyclerView.setLayoutManager(layoutManager);
        viewHolder.recyclerView.setAdapter(adapter);
    }

    @Override
    public void initRangeAdapter(List<String> items) {
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.row_activity_search_range, items.toArray(new String[0]));
        viewHolder.spinner.setAdapter(adapter);
    }

    @Override
    public void updateUserFromLookupsList(List<UserLookup> items) {
        usersLookups.clear();
        usersLookups.addAll(items);
        updateUsersList();
    }

    @Override
    public void updateUserFromGPSList(List<UserLookup> items) {
        usersLookupsGPS.clear();
        usersLookupsGPS.addAll(items);
        updateUsersList();
    }

    public void updateUsersList() {
        users.clear();
        users.addAll(usersLookups);
        users.addAll(usersLookupsGPS);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setGoneVisibilityForList() {
        viewHolder.recyclerView.setVisibility(View.GONE);
    }

    static class ViewHolder {
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @BindView(R.id.activity_search_from_fb)
        View btFromFb;

        @BindView(R.id.activity_search_write_new)
        EditText newFriend;

        @BindView(R.id.activity_search_recycler_view)
        RecyclerView recyclerView;

        @BindView(R.id.activity_search_spinner)
        Spinner spinner;

    }
}
