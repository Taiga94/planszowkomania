package pl.inzyniery.boardgames.ui.createEvent;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import com.dd.processbutton.iml.ActionProcessButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.ui.base.BaseActivity;
import pl.inzyniery.boardgames.ui.login.LoginActivity;
import pl.inzyniery.boardgames.ui.login.LoginPresenter;
import pl.inzyniery.boardgames.ui.search.SearchActivity;

import static android.R.attr.startYear;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class CreateEventActivity extends BaseActivity implements CreateEventPresenter.ViewController {

    private ViewHolder viewHolder;
    private CreateEventPresenter presenter;
    Calendar c = Calendar.getInstance();
    int startYear = c.get(Calendar.YEAR);
    int startMonth = c.get(Calendar.MONTH);
    int startDay = c.get(Calendar.DAY_OF_MONTH);
    Calendar mcurrentTime = Calendar.getInstance();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        viewHolderSetUp();
        presenterSetUp();
    }

    @Override
    protected void viewHolderSetUp() {
        viewHolder = new ViewHolder(getActivityView());
    }

    @Override
    protected void presenterSetUp() {
        presenter = new CreateEventPresenter();
        presenter.onLoad(this);
    }

    @Override
    public void initView() {
        viewHolder.btDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onClickDateButton();
            }
        });
        viewHolder.btAddFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onBtAddFriends();
            }
        });
    }

    @Override
    public void showDateDialog() {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        System.out.println("the selected " + mDay);
        DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year = datePicker.getYear();

                final Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                setDate(calendar);

                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        setDate(calendar, selectedHour, selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Ustaw czas");
                mTimePicker.show();
            }
        }, mYear, mMonth, mDay);
        dialog.show();
    }



    @Override
    public void showTimeDialog() {

    }

    @Override
    public void showInviteFriendActivity() {

    }

    @Override
    public void searchFriendClick() {
        Intent intent = new Intent(CreateEventActivity.this, SearchActivity.class);
        startActivity(intent);
        }


    @Override
    public void showFillInDataInfo() {

    }

    @Override
    public void setDate(Calendar date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        viewHolder.btDate.setText(simpleDateFormat.format(date.getTime()));
    }

    @Override
    public void setDate(Calendar date, int hour, int minute) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        viewHolder.btDate.setText(simpleDateFormat.format(date.getTime()) + " " + hour + ":" + minute);
    }

    static class ViewHolder {
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @BindView(R.id.create_event_button_add_friends_id)
        ActionProcessButton btAddFriends;

        @BindView(R.id.create_event_date_button_id)
        EditText btDate;

        @BindView(R.id.create_event_button_done_id)
        ActionProcessButton btCreateEvent;
    }
}
