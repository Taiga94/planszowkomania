package pl.inzyniery.boardgames.ui.start;

import pl.inzyniery.boardgames.api.interfaces.UniversalLoginListener;
import pl.inzyniery.boardgames.api.engine.ApiEngine;
import pl.inzyniery.boardgames.api.engine.RestEngine;
import pl.inzyniery.boardgames.api.response.UniversalLoginRegisterResponse;
import pl.inzyniery.boardgames.appCore.AppCore;
import pl.inzyniery.boardgames.ui.base.BasePresenter;
import pl.inzyniery.boardgames.util.NetworkUtil;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class StartPresenter extends BasePresenter<StartActivity> {

    protected interface ViewController {
        void initView();

        void startRegister();

        void startLoginActivity();

        void startMainActivity();

        void startProgressDemoButton();

        void stopProgressDemoButton();

        void onErrorProgressButton();

        void clearStateDemoButton();
    }

    @Override
    public void onLoad(StartActivity view) {
        super.onLoad(view);
        getView().initView();
    }

    public void onLoginClicked() {
        getView().startLoginActivity();
    }

    public void onRegisterClicked() {
        getView().startRegister();
    }

    public void onDemoClicked() {
        ApiEngine.sendLoginWithNoAccount(NetworkUtil.getMacAddress(), new UniversalLoginListener() {
            @Override
            public void onSuccess(UniversalLoginRegisterResponse response) {
                AppCore.setUserName(response.getName());
                RestEngine.setToken(response.getToken());
                getView().startMainActivity();
            }

            @Override
            public void onTaskStart() {
                getView().clearStateDemoButton();
                getView().startProgressDemoButton();
            }

            @Override
            public void onTaskEnd() {
                getView().stopProgressDemoButton();
                getView().clearStateDemoButton();
            }

            @Override
            public void onFailure() {
                getView().onErrorProgressButton();
            }
        });
    }
}
