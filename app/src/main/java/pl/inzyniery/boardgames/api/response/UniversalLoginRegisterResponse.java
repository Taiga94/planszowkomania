package pl.inzyniery.boardgames.api.response;

import com.google.gson.annotations.SerializedName;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class UniversalLoginRegisterResponse {

    @SerializedName("token")
    String token;

    @SerializedName("name")
    String name;

    public UniversalLoginRegisterResponse(String token, String name) {
        this.token = token;
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
