package pl.inzyniery.boardgames.ui.duel;

import pl.inzyniery.boardgames.ui.base.BasePresenter;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class DuelPresenter extends BasePresenter<DuelActivity>  {

    public interface ViewController {
        void initView();
        void searchRivalClick();
    }

    @Override
    public void onLoad(DuelActivity view) {
        super.onLoad(view);
        getView().initView();
    }

    public void onBtSearchRival() {
        getView().searchRivalClick();
    }
}
