package pl.inzyniery.boardgames.ui.chat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.inzyniery.boardgames.NotificationCenter;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.api.engine.RestEngine;
import pl.inzyniery.boardgames.api.request.ChatDetailsRequest;
import pl.inzyniery.boardgames.api.response.ChatDetailsResponse;
import pl.inzyniery.boardgames.api.response.MessageResponse;
import pl.inzyniery.boardgames.appCore.AppCore;
import pl.inzyniery.boardgames.util.AndroidUtilities;
import pl.inzyniery.boardgames.widgets.SizeNotifierRelativeLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;


public class ChatActivity extends ActionBarActivity implements SizeNotifierRelativeLayout.SizeNotifierRelativeLayoutDelegate {

    private ListView chatListView;
    private EditText chatEditText1;
    private ArrayList<ChatMessage> chatMessages;
    private ImageView enterChatView1;
    private ChatListAdapter listAdapter;
    private SizeNotifierRelativeLayout sizeNotifierRelativeLayout;
    private String topic;
    private WebService webService;
    private MqttAndroidClient client;
    private ObjectMapper objectMapper = new ObjectMapper();


    private EditText.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press

                EditText editText = (EditText) v;

                if(v==chatEditText1)
                {
                    sendMessage(editText.getText().toString(), UserType.OTHER);
                }

                chatEditText1.setText("");

                return true;
            }
            return false;

        }
    };

    private ImageView.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(v==enterChatView1)
            {
                sendMessage(chatEditText1.getText().toString(), UserType.OTHER);
            }

            chatEditText1.setText("");

        }
    };

    private final TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (chatEditText1.getText().toString().equals("")) {

            } else {
                enterChatView1.setImageResource(R.drawable.ic_chat_send);

            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(editable.length()==0){
                enterChatView1.setImageResource(R.drawable.ic_chat_send);
            }else{
                enterChatView1.setImageResource(R.drawable.ic_chat_send_active);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        AndroidUtilities.statusBarHeight = getStatusBarHeight();

        chatMessages = new ArrayList<>();

        chatListView = (ListView) findViewById(R.id.chat_list_view);

        chatEditText1 = (EditText) findViewById(R.id.chat_edit_text1);
        enterChatView1 = (ImageView) findViewById(R.id.enter_chat1);

        listAdapter = new ChatListAdapter(chatMessages, this);

        chatListView.setAdapter(listAdapter);

        chatEditText1.setOnKeyListener(keyListener);

        enterChatView1.setOnClickListener(clickListener);

        chatEditText1.addTextChangedListener(watcher1);

        sizeNotifierRelativeLayout = (SizeNotifierRelativeLayout) findViewById(R.id.chat_layout);
        sizeNotifierRelativeLayout.delegate = this;

        NotificationCenter.getInstance().addObserver(this, NotificationCenter.emojiDidLoaded);

        topic = "chatRequest/1";
        buildRetrofit();

        String clientId = MqttClient.generateClientId();
        client = new MqttAndroidClient(this.getApplicationContext(), "tcp://192.168.43.177", clientId);
        try {
            IMqttToken token = client.connect();
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    Log.d("ChatActivity", new String(message.getPayload(), "UTF-8"));
                    MessageResponse response = objectMapper.readValue(message.getPayload(), MessageResponse.class);
                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.setMessageText(response.getTextMessage());
                    if (response.getAuthor().equals(AppCore.getUserName())) {
                        chatMessage.setUserType(UserType.SELF);
                    } else {
                        chatMessage.setUserType(UserType.OTHER);
                    }
                    chatMessage.setAuthor(response.getAuthor());
                    chatMessage.setMessageTime(response.getTime());
                    chatMessages.add(chatMessage);

                    ChatActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            listAdapter.notifyDataSetChanged();
                            scrollMyListViewToBottom();
                        }
                    });
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                }
            });

            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.d("ChatActivity", "onSuccess - MQTT");
                    int qos = 1;
                    topic = "chatResponse/1";
                    try {
                        client.subscribe(topic, qos);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Log.d("ChatActivity", "onFailure - MQTT");
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
        initMessages();
    }

    private void sendMessage(final String messageText, final UserType userType)
    {
        String topic = "chatRequest/1";
        JSONObject messageJson = createMessage(messageText);
        String payload = messageJson.toString();
        byte[] encodedPayload;
        try {
            encodedPayload = payload.getBytes("UTF-8");
            MqttMessage message = new MqttMessage(encodedPayload);
            client.publish(topic, message);
        } catch (UnsupportedEncodingException | MqttException e) {
            e.printStackTrace();
        }

    }


    private JSONObject createMessage(String messageText) {
        JSONObject message = new JSONObject();
        try {
            message.put("author", AppCore.getUserName());
            message.put("textMessage", messageText);
            message.put("tokenContent", RestEngine.getToken());
            return message;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void initMessages() {
        final String headerInfo = RestEngine.getToken();
        final ChatDetailsRequest chatDetailsRequest = new ChatDetailsRequest();
        chatDetailsRequest.setId("1");
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                        Request request = chain.request();
                        request = request.newBuilder()
                                .addHeader("token", headerInfo)
                                .build();
                        okhttp3.Response response = chain.proceed(request);
                        return response;
                    }
                })
                .addInterceptor(interceptor)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestEngine.URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(client)
                .build();
        WebService webService = retrofit.create(WebService.class);
        Call<ChatDetailsResponse> call = webService.getChatDetails(chatDetailsRequest);
        call.enqueue(new Callback<ChatDetailsResponse>() {
            @Override
            public void onResponse(Call<ChatDetailsResponse> call, Response<ChatDetailsResponse> response) {
                List<MessageResponse> messagesList = response.body().getMessages();
                for(int i = 0; i < messagesList.size(); i++) {
                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.setMessageText(messagesList.get(i).getTextMessage());
                    if (messagesList.get(i).getAuthor().equals(RestEngine.getToken())) {
                        chatMessage.setUserType(UserType.SELF);
                    } else {
                        chatMessage.setUserType(UserType.OTHER);
                    }

                    chatMessage.setMessageTime(messagesList.get(i).getTime());
                    chatMessages.add(chatMessage);

                }
                ChatActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        listAdapter.notifyDataSetChanged();
                        scrollMyListViewToBottom();
                    }
                });
            }

            @Override
            public void onFailure(Call<ChatDetailsResponse> call, Throwable t) {

            }
        });
    }

    private Activity getActivity()
    {
        return this;
    }



    @Override
    public void onSizeChanged(int height) {

        Rect localRect = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(localRect);

        WindowManager wm = (WindowManager) AppCore.getInstance().getSystemService(Activity.WINDOW_SERVICE);
        if (wm == null || wm.getDefaultDisplay() == null) {
            return;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        NotificationCenter.getInstance().removeObserver(this, NotificationCenter.emojiDidLoaded);
    }

    /**
     * Get the system status bar height
     * @return
     */
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private void buildRetrofit() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestEngine.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        webService = retrofit.create(WebService.class);
    }

    private void scrollMyListViewToBottom() {
        chatListView.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                chatListView.setSelection(listAdapter.getCount() - 1);
            }
        });
    }


}