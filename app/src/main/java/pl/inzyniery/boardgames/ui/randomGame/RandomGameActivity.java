package pl.inzyniery.boardgames.ui.randomGame;

import android.os.Bundle;
import android.support.annotation.Nullable;

import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.ui.base.BaseActivity;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class RandomGameActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_layout);
    }

    @Override
    protected void viewHolderSetUp() {

    }

    @Override
    protected void presenterSetUp() {

    }
}
