package pl.inzyniery.boardgames.ui.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.ui.createEvent.CreateEventActivity;
import pl.inzyniery.boardgames.ui.duel.DuelActivity;
import pl.inzyniery.boardgames.ui.fastGame.QuickGameActivity;
import pl.inzyniery.boardgames.ui.message.MessageActivity;
import pl.inzyniery.boardgames.ui.randomGame.RandomGameActivity;
import pl.inzyniery.boardgames.ui.tournament.TournamentActivity;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class GamesAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ItemGame> games;
    private ViewHolder viewHolder;

    public GamesAdapter(Context context, ArrayList<ItemGame> games) {
        this.context = context;
        this.games = games;
    }

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public ItemGame getItem(int i) {
        return games.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ItemGame game = games.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_game_grid, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickItem(view, position);
            }
        });
        viewHolder.tvTite.setText(game.getTitle());
        viewHolder.civImage.setImageDrawable(getDrawableItem(game.getDrawableRes()));
        return convertView;
    }

    private Drawable getDrawableItem(int res) {
        return ResourcesCompat.getDrawable(context.getResources(), res, null);
    }

    private void onClickItem(View view, final int position) {

        Intent intent;
        switch (position) {
            case 0:
                intent = new Intent(context, TournamentActivity.class);
                context.startActivity(intent);
                break;
            case 1:
                intent = new Intent(context, DuelActivity.class);
                context.startActivity(intent);
                break;
            case 2:
                intent = new Intent(context, RandomGameActivity.class);
                context.startActivity(intent);
                break;
            case 3:
                intent = new Intent(context, CreateEventActivity.class);
                context.startActivity(intent);
                break;
            case 4:
                intent = new Intent(context, MessageActivity.class);
                context.startActivity(intent);
                break;
            default:
                Toast.makeText(context, R.string.in_dev, Toast.LENGTH_SHORT).show();
        }
    }


    static class ViewHolder {
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @BindView(R.id.item_civ)
        CircleImageView civImage;

        @BindView(R.id.item_text)
        TextView tvTite;
    }
}
