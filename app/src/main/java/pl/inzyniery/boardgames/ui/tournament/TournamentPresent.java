package pl.inzyniery.boardgames.ui.tournament;

import pl.inzyniery.boardgames.ui.base.BasePresenter;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class TournamentPresent extends BasePresenter<TournamentActivity> {
    protected interface ViewController {
        void initView();
        void clickFresh();

    }

    @Override
    public void onLoad(TournamentActivity view) {
        super.onLoad(view);
        getView().initView();
    }

    public void onBtRefreach() {
        getView().clickFresh();
    }
}
