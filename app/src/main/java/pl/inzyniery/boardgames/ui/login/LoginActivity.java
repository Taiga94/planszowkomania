package pl.inzyniery.boardgames.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.dd.processbutton.iml.ActionProcessButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.inzyniery.boardgames.R;
import pl.inzyniery.boardgames.ui.base.BaseActivity;
import pl.inzyniery.boardgames.ui.search.SearchActivity;

/**
 * ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
 * ░░░░░ ░░░░▀█▄▀▄▀██████░▀█▄▀▄▀████▀
 * ░░░░ ░░░░░░░▀█▄█▄███▀░░░▀██▄█▄█▀
 */

public class LoginActivity extends BaseActivity implements LoginPresenter.ViewController {

    private ViewHolder viewHolder;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        viewHolderSetUp();
        presenterSetUp();
    }

    @Override
    protected void viewHolderSetUp() {
        viewHolder = new ViewHolder(getActivityView());
    }

    @Override
    protected void presenterSetUp() {
        presenter = new LoginPresenter();
        presenter.onLoad(this);
    }

    @Override
    public void initView() {
        viewHolder.btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onBtLoginClicked(
                        viewHolder.etLogin.getText().toString(),
                        viewHolder.etPassword.getText().toString()
                );
            }
        });

        viewHolder.btFbLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onFbBtLogin();
            }
        });

        setViewRippleEffect(viewHolder.btFbLogin);
        setViewRippleEffect(viewHolder.btLogin);
        viewHolder.btLogin.setMode(ActionProcessButton.Mode.ENDLESS);
    }

    @Override
    public void setButtonProgress() {
        viewHolder.btLogin.setProgress(1);
    }

    @Override
    public void stopButtonProgress() {
        viewHolder.btLogin.setProgress(0);
    }

    @Override
    public void showErrorButton() {
        viewHolder.btLogin.setProgress(-1);
        showNegativeToast(R.string.login_error);
    }

    @Override
    public void showFillInDataInfo() {
        showNegativeToast(R.string.empty_data);
    }

    static class ViewHolder {
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @BindView(R.id.login_activity_bt_login)
        ActionProcessButton btLogin;

        @BindView(R.id.avitivty_login_bt_fb_login)
        LinearLayout btFbLogin;

        @BindView(R.id.activity_login_et_login)
        EditText etLogin;

        @BindView(R.id.activity_login_et_password)
        EditText etPassword;
    }
}
